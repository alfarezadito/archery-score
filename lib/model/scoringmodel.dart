import 'package:flutter/material.dart';

class ScoringModel{
  String date;
  
  static List<String> division =[
    'Recurve',
    'Compound',
    'Standart Bow'
  ];
  
  static List<int> distance =[
    90,70,60,50,30,20,18,15
  ];

  static List<int> arrow =[
    3,6,12
  ];

  static Map target = {
    "Full target 122cm" : "assets/images/target.png",
    "Target 6 ring" : "assets/images/target-5ring.png"
  };
  
  
}

@immutable
class Item{
  static int id;
  // static 
}