import 'package:archery_score/ui/screen/login.dart';
import 'package:archery_score/ui/screen/register.dart';
import 'package:archery_score/ui/screen/scoringnewform.dart';
import 'package:archery_score/ui/screen/scoringsession.dart';
import 'package:archery_score/ui/screen/targetlist.dart';
import 'package:archery_score/ui/screen/timersetting.dart';
import 'package:flutter/material.dart';
import 'package:archery_score/ui/screen/dashboardScreen.dart';
import 'package:archery_score/ui/screen/shootingtimer.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => ShootTime(),
    child: ScoringApp(),
  ));
}

class ScoringApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Archery Score',
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.transparent,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginScreen(),
        '/register': (context) => RegisterScreen(),
        '/home': (context) => Dashboard(),
        '/timer': (context) => ShootingTimer(),
        '/timerset': (context) => TimerSettings(),
        '/newscore': (context) => ScoringNewForm(),
        '/targetList': (context) => TargetList(),
        '/scoreSession': (context) => ScoringSession()
      },
    );
  }
}
