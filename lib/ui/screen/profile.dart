import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:archery_score/model/TestProfile.dart';
import 'package:http/http.dart' as http;

Future<Profile> fetchProfile() async {
  final response = await http.get('http://192.168.1.2/mobile/test.php');
  return Profile.fromJson(jsonDecode(response.body));
}

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Future<Profile> myProfile;

  @override
  void initState() {
    super.initState();
    myProfile = fetchProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<Profile>(
      future: myProfile,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: [
              Text(
                snapshot.data.name,
                style: TextStyle(fontSize: 20, color: Colors.white)
              ),
              Text(
                snapshot.data.email,
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            ],
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    ));
  }
}
