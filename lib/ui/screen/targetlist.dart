import 'package:archery_score/ui/component/loginstyle.dart';
import 'package:flutter/material.dart';

class TargetList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Target List',
          style: kLabelStyle,
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF0A83F2),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/target.png'),
              ),
              title: Text('Full Target 122cm'),
              onTap: () {},
            ),
            Divider(
              height: 15,
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/target-5ring.png'),
              ),
              title: Text('Target 6 Ring'),
              onTap: () {},
            ),
            Divider(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
