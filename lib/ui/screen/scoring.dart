import 'package:flutter/material.dart';

class ScoringDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 15,
          ),
          Text(
            'Score History',
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          ListTile(
            
            tileColor: Colors.white,
            title: Text("Recurve 70M"),
            subtitle: Text("30/11/2020"),
            trailing: Text("312/360"),
          ),
          Divider(
            height: 10,
          ),
          ListTile(
            tileColor: Colors.white,
            title: Text("Standart Bow 40M"),
            subtitle: Text("22/11/2020"),
            trailing: Text("276/360"),
            
          )
          // ListView.separated(
          //   shrinkWrap: true,

          //   itemBuilder: (context, index){
          //   return ListTile(
          //     tileColor: Colors.white,
          //     title: Text("Recurve 70M"),
          //     subtitle: Text("30/11/2020"),
          //     trailing: Text("312/360"),
          //   );
          // }, separatorBuilder: (context, index){
          //   return Divider(height: 10, color: Colors.white,);
          // }, itemCount: 3)
        ],
      ),
    );
  }
}
