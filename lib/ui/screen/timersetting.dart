import 'package:archery_score/ui/component/loginstyle.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

@override
// ignore: must_be_immutable
class TimerSettings extends StatelessWidget {
  TextEditingController shootingTime = new TextEditingController();
  // int shoot = 240;

  popUpTimerSettings(BuildContext context) {
    AlertDialog alertDialog = new AlertDialog(
      backgroundColor: Colors.white,
      title: Text("Enter time",
          style: TextStyle(
            fontFamily: 'OpenSans',
            fontSize: 20,
            fontWeight: FontWeight.bold,
          )),
      content: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: TextField(
          controller: shootingTime,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () {
              var time = context.read<ShootTime>();
              time.setTime(int.parse(shootingTime.text));
              Navigator.pop(context);
            },
            child: Text('OK'))
      ],
    );
    showDialog(child: alertDialog, context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Timer Settings',
          style: kLabelStyle,
        ),
        backgroundColor: Color(0xFF0A83F2),
      ),
      body: Container(
        child: Column(
          children: [
            ListTile(
              title: Text('Shooting Time'),
              subtitle: Consumer<ShootTime>(
                builder: (context, shoottime, child) => Text(
                  '${shoottime.time}',
                ),
              ),
              onTap: () {
                popUpTimerSettings(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ShootTime with ChangeNotifier {
  int time = 240;

  void setTime(int t) {
    time = t;
    notifyListeners();
  }
}
