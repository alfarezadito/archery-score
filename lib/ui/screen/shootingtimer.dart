import 'dart:async';
// import 'package:provider/provider.dart';
// import 'package:archery_score/ui/screen/timersetting.dart';
import 'package:flutter/material.dart';

class ShootingTimer extends StatefulWidget {
  @override
  _ShootingTimerState createState() => _ShootingTimerState();
}

// class GetTime extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // var shoottime = context.watch<ShootTime>();
//     return Text(context.watch<ShootTime>());
//   }
// }

class _ShootingTimerState extends State<ShootingTimer> {
  // Countdown Timer
  bool _start = false;
  int _time = 120;
  Timer _timer;
  void startTimer() {
    const sec = const Duration(seconds: 1);
    _timer = new Timer.periodic(sec, (Timer timer) {
      if (_time == 0) {
        setState(() {
          timer.cancel();
          _start = false;
        });
      } else {
        setState(() {
          _time--;
        });
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (_start) {
            _start = false;
          } else {
            _start = true;
          }
        });
        startTimer();
      },
      child: Scaffold(
        backgroundColor: _start ? Colors.green : Colors.red,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.pushNamed(context, '/timerset');
              },
            )
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Touch anywhere to start timer',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  // fontFamily: 'OpenSans',
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                '$_time',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  // fontFamily: 'OpenSans',
                  fontSize: 30,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
