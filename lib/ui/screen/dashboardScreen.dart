import 'package:archery_score/ui/component/loginstyle.dart';
import 'package:archery_score/ui/screen/club.dart';
import 'package:archery_score/ui/screen/performance.dart';
import 'package:archery_score/ui/screen/profile.dart';
import 'package:archery_score/ui/screen/scoring.dart';
import 'package:flutter/material.dart';
import 'package:archery_score/ui/component/drawer.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int curIndex = 0;
  List<Widget> widgetController = <Widget>[
    ScoringDashboard(),
    ClubScreen(),
    PerformanceScreen(),
    ProfileScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/bg-scaffold.png'),fit: BoxFit.contain
        )
      ),
      child: Scaffold(
        
        appBar: AppBar(
          centerTitle: true,
          title: Text('Archery Score', style: kLabelStyle,),
          backgroundColor: Color(0xFF0A83F2),
          actions: <Widget>[IconButton(icon: Icon(Icons.show_chart), onPressed: ()=>print('to graph'),)],
        ),

        drawer: drawerApp(context),
        
        body: Center(
          child: widgetController.elementAt(curIndex),
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, '/newscore');
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.blueAccent,
        ),


        bottomNavigationBar: BottomNavigationBar(
          currentIndex: curIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF0A83F2),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Scoring',
              // backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.supervised_user_circle_rounded),
              label: 'Club',
              // backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              // icon: SvgPicture.asset('assets/icons/graph2.svg', color: Colors.black54,),
              icon: Icon(Icons.show_chart),
              label: 'Performance',
              // backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_sharp),
              label: 'Profile',
              // backgroundColor: Colors.blue,
            ),
          ],
          selectedItemColor: Colors.white,
          onTap: (index) {
            setState(() {
              curIndex = index;
            });
          },
        ),
      ),
    );
  }
}
