import 'package:archery_score/ui/component/loginstyle.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

buildRow(int round) {
  return TableRow(children: [
    Center(
        child: Text(
      "$round",
      style: TextStyle(
          fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xFF0A83F2)),
    )),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
      child: TextField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
      ),
    ),
    Center(
        child: Text(
      "=",
      style: TextStyle(
          fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xFF0A83F2)),
    )),
    Center(
        child: Text(
      "total",
    )),
  ]);
}

class ScoringSession extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Scoring',
          style: kLabelStyle,
        ),
        backgroundColor: Color(0xFF0A83F2),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName('/home'));
              }),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/target.png'),
                ),
                title: Text("Recurve 70M"),
                subtitle: Text("30/11/2020"),
                trailing: Text("0/360"),
              ),
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                // border: TableBorder.all(color: Color(0xFF0A83F2)),
                children: [
                  buildRow(1),
                  buildRow(2),
                  buildRow(3),
                  buildRow(4),
                  buildRow(5),
                  buildRow(6),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(2.0),
                child: Text('Total score', textAlign: TextAlign.end),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
