import 'package:archery_score/ui/component/loginstyle.dart';
import 'package:flutter/material.dart';

class ScoringNewForm extends StatefulWidget {
  @override
  _ScoringNewFormState createState() => _ScoringNewFormState();
}

class _ScoringNewFormState extends State<ScoringNewForm> {
  DateTime _dateTime;
  
  List<String> division = ["Recurve", "Compound", "Standart Bow", "Barebow"];
  List<String> distance = ["70", "50", "40"];
  List<String> numarrow = ["12", "6", "3"];
  String _numarrow;
  String _division;
  String _distance;
  
  void setDate(DateTime date){
    setState(() {
      _dateTime = date;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          );
        }),
        title: Text(
          'New Scoring',
          style: kLabelStyle,
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF0A83F2),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                Navigator.pushNamed(context, '/scoreSession');
              }),
        ],
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Fill your scoring specification bellow :',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                fontFamily: 'OpenSans',
              ),
            ),
            Divider(
              height: 30,
              thickness: 5,
              color: Color(0xFF0A83F2),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  'Date :',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'OpenSans'),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                    _dateTime == null ? 'Select a date': "${_dateTime.day}/${_dateTime.month}/${_dateTime.year}"),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.date_range),
                    color: Colors.black54,
                    onPressed: () {
                      showDatePicker(
                        context: context,
                        initialDate: _dateTime==null ? DateTime.now() : _dateTime,
                        firstDate: DateTime(2010),
                        lastDate: DateTime(2025),
                      ).then((date) {
                        setDate(date);
                      });
                    },
                  ),
                )
              ],
            ),
            Divider(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  'Division :',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'OpenSans'),
                ),
                SizedBox(
                  width: 20,
                ),
                DropdownButton(
                    hint: Text('Select a division'),
                    value: _division,
                    items: division.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _division = value;
                      });
                    })
              ],
            ),
            Divider(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  'Distance :',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'OpenSans'),
                ),
                SizedBox(
                  width: 20,
                ),
                DropdownButton(
                    hint: Text('Select distance'),
                    value: _distance,
                    items: distance.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _distance = value;
                      });
                    }),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Meters',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'OpenSans'),
                ),
              ],
            ),
            Divider(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  'Arrows each round :',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'OpenSans'),
                ),
                SizedBox(
                  width: 20,
                ),
                DropdownButton(
                    hint: Text('Number of arrows'),
                    value: _numarrow,
                    items: numarrow.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _numarrow = value;
                      });
                    }),
              ],
            ),
            Divider(
              height: 20,
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/target.png'),
              ),
              title: Text('Target size'),
              onTap: () {
                Navigator.pushNamed(context, '/targetList');
              },
            ),
          ],
        ),
      ),
    );
  }
}
