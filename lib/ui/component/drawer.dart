import 'package:flutter/material.dart';

Widget drawerApp(context) => Drawer(
  child: ListView(
          padding : EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
                // color: Colors.grey,
                // margin: EdgeInsets.all(0),
                padding: EdgeInsets.zero,
                height: 20,
                width: 20,
                alignment: Alignment(0,1.14),
                // padding: EdgeInsets.only(right: 0),
                child: SizedBox(
                  height: 20,
                  width: double.infinity,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: Colors.blue
                    ),
                    child: Text(
                      'Alfareza Harnandito', 
                      textAlign: TextAlign.center, 
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                      ),
                    ),
                  ),
                  
                ),
              ),
              decoration: BoxDecoration(
                color: Color(0xFF0A83F2),
                image: DecorationImage(
                  image: AssetImage('assets/images/bg-home.png'),
                  fit: BoxFit.cover
                )
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Dashboard Score'),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.timer),
              title: Text('Shooting Timer',),
              onTap: (){
                Navigator.pushNamed(context, '/timer');
              },
            ),
            Divider(height: 15,thickness: 2,indent: 15,endIndent: 15,),
            // SizedBox(height: 50),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Log Out'),
              onTap: () {
                Navigator.pushReplacementNamed(context, '/');
              },
              // tileColor: Colors.blue[900]
            ),
          ],
        ),
);